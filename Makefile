##############################################################################
# Makefile for 'metconv', written by Jesus Ortega (a.k.a. Nitebirdz)         #
# This file should work fine in Linux.  I will make the necessary changes    #
# sooner or later so that it also works on other platforms.                  #
##############################################################################

# Default compiler and options (with extra warnings and debugging)
CC = gcc
CFLAGS    = -Wall -g -o

# Pass these variables to 'make' subprocesses
export    CFLAGS

# Other variables
OWNER = root
GROUP = root
PREFIX = /usr/local
EXEC_PREFIX = $(PREFIX)
BINDIR = $(EXEC_PREFIX)/bin
MANDIR = /usr/share/man
MAN1DIR = $(MANDIR)/man1

# Define targets
all: 
	$(CC) $(CFLAGS) metconv metconv.c

install: metconv man/metconv.1.gz
	install -c -o $(OWNER) -g $(GROUP) -m 755 metconv $(BINDIR)
	install -c -o $(OWNER) -g $(GROUP) -m 644 man/metconv.1.gz $(MAN1DIR)

uninstall:
	/bin/rm -f $(BINDIR)/metconv
	/bin/rm -f $(MAN1DIR)/metconv.1.gz
 
clean:
	/bin/rm -f metconv
