/************************************************************************
* Filename: metconv.c (metconv)                                         *
* Author: Jesus Ortega (a.k.a. Nitebirdz) <nitebirdz@sacredchaos.com>   *
* v0.4 (10 February 2003)                                               *
* Purpose: command based application that converts measurements from    *
*       the metric system into the standard system and viceversa.  The  *
*       program handles celsius, farenheit, kilometers, miles, meters,  *
*       feet, and inches.                                               *
************************************************************************/
/* TODO:
 - Add conversion for shoe sizes, perhaps clothing too
*/


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <errno.h>
#include <getopt.h>
#undef DEBUG

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif


/***************************************************
 * Global variables                                *
 **************************************************/
float units;    /* second argument passed to command in order to convert */
const char *program_name;    /* program name, for error messages */
int i;
char option2[BUFSIZ];
char version_number[] = "0.3.1";

/***************************************************
 * Prototypes                                      *
 **************************************************/
/* void print_usage(const, int); */
int main(int argc, char **argv);
/* int process_args(int argc, char **argv);
 int isnum(char *); */
int convert_farenheit(float);
int convert_celsius(float);
int convert_kilometers(float);
int convert_miles(float);
int convert_kilograms(float);
int convert_pounds(float);
int convert_feet(float);
int convert_meters(float);
int convert_inches(float);
int convert_centimeters(float);


/***************************************************
 * Usage - tells the user how to use the app       *
 **************************************************/
void print_usage (FILE* stream, int exit_code) {
  fprintf (stream, "Usage: %s [option] [units]\n", program_name);
  fprintf (stream,
      " -h  --help          Display this usage information.\n"
      " -V  --version       Print version.\n"
      " -K  --kilometers    Convert kilometers.\n"
      " -M  --miles         Convert miles.\n"
      " -k  --kilograms     Convert kilograms.\n"
      " -P  --pounds        Convert pounds.\n"
      " -C  --celsius       Convert celsius.\n"
      " -F  --farenheit     Convert farenheit.\n"
      " -f  --feet          Convert feet.\n"
      " -m  --meters        Convert meters.\n"
      " -i  --inches        Convert inches.\n"
      " -c  --centimeters   Convert centimeters.\n");
  exit (exit_code);
}


/***************************************************
 * Version - print program version                 *
 * ************************************************/
void print_version(FILE* stream, int exit_code) {
  /* FIXME: make version a variable at the top of the program */
  fprintf(stream, "%s version %s\n", program_name, version_number);
  exit(exit_code);
}


/***************************************************
 * Main function                                   *
 **************************************************/
int main(int argc, char *argv[]) {
  /* save name of the program for errors */
  program_name = argv[0];

  int next_option;    /* used to parse through command arguments */

  const char* const short_options = "hVK:M:k:P:C:F:f:m:i:c:";
  
  /* list of accepted options */
  const struct option long_options[] = {
    { "help",         0, NULL, 'h' },
    { "version",      0, NULL, 'V' },
    { "kilometers",   1, NULL, 'K' },
    { "miles",        1, NULL, 'M' },
    { "kilograms",    1, NULL, 'k' },
    { "pounds",       1, NULL, 'P' },
    { "celsius",      1, NULL, 'C' },
    { "farenheit",    1, NULL, 'F' },
    { "feet",         1, NULL, 'f' },
    { "meters",       1, NULL, 'm' },
    { "inches",       1, NULL, 'i' },
    { "centimeters",  1, NULL, 'c' },
    { NULL,           0, NULL, 0   }   /* required for end of array */
  };


#ifdef DEBUG
  fprintf(stderr, "DEBUG: right before parsing arguments\n");
#endif
  
  do {
    next_option = getopt_long (argc, argv, short_options, long_options, NULL);

    switch (next_option) {
      case 'h':   /* display usage information */
        print_usage(stdout, 0);

      case 'V':   /* print version */
        print_version(stdout, 0);

      case 'K':   /* kilometers */
        units = atof(optarg);
        convert_kilometers(units);
        break;
        
      case 'M':   /* miles */
        units = atof(optarg);
        convert_miles(units);
        break;
        
      case 'k':   /* kilograms */
        units = atof(optarg);
        convert_kilograms(units);
        break;
        
      case 'P':   /* pounds */
        units = atof(optarg);
        convert_pounds(units);
        break;
        
      case 'C':   /* celsius */
        units = atof(optarg);
        convert_celsius(units);
        break;
        
      case 'F':   /* farenheit */
        units = atof(optarg);
        convert_farenheit(units);
        break;
        
      case 'f':   /* feet */
        units = atof(optarg);
        convert_feet(units);
        break;
        
      case 'm':   /* meters */
        units = atof(optarg);
        convert_meters(units);
        break;
        
      case 'i':    /* inches */
        units = atof(optarg);
        convert_inches(units);
        break;
        
      case 'c':    /* centimeters */
        units = atof(optarg);
        convert_centimeters(units);
        break;
        
      case '?':    /* user specified invalid option */
        print_usage(stderr, 1);

      case -1:    /* done with the options */
        break;

      default:    /* something unexpected */
        abort ();
    }
  }
  while (next_option != -1);  
  
  return 0;
}


/***************************************************
* Convert Farenheit to Celsius                     *
***************************************************/
int convert_farenheit(float farenheit) {

  /* Define local variables */
  float celsius;
  
  /* Convert Farenheit into Celsius */
  celsius = 5 * ( farenheit  - 32 ) / 9;

  /* Display output in stdout */
  printf("Farenheit: %.2f\n", farenheit);
  printf("Celsius: %.2f\n", celsius);
  return(0);

}

/***************************************************
* Convert Celsius to Farenheit                     *
***************************************************/
int convert_celsius(float celsius) {

  /* Define local variables */
  float farenheit;

  /* Convert Celsius to Farenheit */
  farenheit = ( celsius * 9 ) / 5 + 32;

  /* Display output in stdout */
  printf("Celsius: %.2f\n", celsius);
  printf("Farenheit: %.2f\n", farenheit);
  return(0);

}

/***************************************************
* Convert Kilometers to Miles                      *
***************************************************/
int convert_kilometers(float kilometers) {

  /* Define local variables */
  float miles;

  /* Convert Kilometers to Miles */
  miles = kilometers / 1.609;

  /* Display output in stdout */
  printf("Kilometers: %.3f\n", kilometers);
  printf("Miles: %.3f\n", miles);
  return(0);

}

/****************************************************
* Convert Miles to Kilometers                       *
****************************************************/
int convert_miles(float miles) {

  /* Define local variables */
  float kilometers;

  /* Convert Miles to Kilometers */
  kilometers = miles * 1.609;

  /* Display output in stdout */
  printf("Miles: %.3f\n", miles);
  printf("Kilometers: %.3f\n", kilometers);
  return(0);

}

/***************************************************
* Convert Kilograms to Pounds                      *
***************************************************/
int convert_kilograms(float kilograms) {

  /* Define local variables */
  float pounds;

  /* Convert Kilograms to Pounds */
  pounds = kilograms / 0.454;

  /* Display output in stdout */
  printf("Kilograms: %.2f\n", kilograms);
  printf("Pounds: %.2f\n", pounds);
  return(0);

}

/***************************************************
* Convert Pounds to Kilograms                      *
***************************************************/
int convert_pounds(float pounds) {

  /* Define local variables */
  float kilograms;

  /* Convert Pounds to Kilograms */
  kilograms = pounds * 0.454;

  /* Display output in stdout */
  printf("Pounds: %.2f\n", pounds);
  printf("Kilograms: %.2f\n", kilograms);
  return(0);  


}

/***************************************************
* Convert Feet to Meters                           *
***************************************************/
int convert_feet(float feet) {

  /* Define local variables */
  float meters;

  /* Convert Feet to Meters */
  meters = feet * 0.3048;

  /* Display output in stdout */
  printf("Feet: %.3f\n", feet);
  printf("Meters: %.3f\n", meters);
  return(0);

}

/***************************************************
* Convert Meters to Feet                           *
***************************************************/
int convert_meters(float meters) {

  /* Define local variables */
  float feet;

  /* Convert Meters to Feet */
  feet = meters / 0.3048;

  /* Display output in stdout */
  printf("Meters: %.3f\n", meters);
  printf("Feet: %.3f\n", feet);
  return(0);
  
}

/***************************************************
* Convert Inches to Centimeters                    *
***************************************************/
int convert_inches(float inches) {

  /* Define local variables */
  float centimeters;

  /* Convert Inches to Centimeters */
  centimeters = inches * 2.54;

  /* Display output in stdour */
  printf("Inches: %.3f\n", inches);
  printf("Centimeters: %.3f\n", centimeters);
  return(0);

}

/***************************************************
* Convert Centimeters to Inches                    *
***************************************************/
int convert_centimeters(float centimeters) {

  /* Define local variables */
  float inches;

  /* Convert Centimeters to Inches */
  inches = centimeters / 2.54;

  /* Display output in stdout */
  printf("Centimeters: %.3f\n", centimeters);
  printf("Inches: %3.f\n", inches);
  return(0);

}

